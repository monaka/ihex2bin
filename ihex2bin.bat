@echo off
if "%1" == "" goto error

bfin-elf-objcopy -Iihex -Obinary %1
goto end
:error
echo ihex2bin [LDR file (ihex format)]
:end
