ihex2bin.bat 取り扱いメモ
										May 29, 2009
								合資会社もなみソフトウェア

■ihex2bin.bat の動作環境
ihex2bin.bat は、32bit版Windows XPがインストールされているPC上で動作します。
64bit版および別バージョンのWindowsでも動作すると思われますが、未確認です。

■ihex2bin.bat のインストール
配布用のzipファイルに含まれるihex2bin.batを、ローカルディスク上の任意の場所に配置します。
ihex2bin は、内部で bfin-elf-objcopy.exe コマンドを呼び出しています。
bfin-elf-objcopy.exe コマンドは、ihex2bin.bat と同じディレクトリか、PATH環境変数に列挙されている場所に配置してください。

■ihex2bin.bat の使用方法
ihex形式のldrファイルのファイル名が test.ldr だったとき、

ihex2bin test.ldr

を実行すると、binary形式の test.ldr に変換されます。
元の(ihex形式の) ldrファイルは保存されませんので、必要に応じ事前にバックアップを取ってください。



■bfin-elf-objcopy.exe の再配布について
bfin-elf-objcopy.exe は、GNU binutils に含まれており、GNU GPL に基づく自由な再配布が許諾されています。
ソースコード一式は http://pf3gnuchains.sourceforge.jp に記述されている方法で取得することができます。


■ihex2bin.batおよびbfin-elf-objcopy.exeに関する無保証について
ihex2bin.bat および bfin-elf-objcopy.exe は、富士エレクトロニクス製フラッシュライタの活用のための参考情報として提供されるものであり、
これらのソフトウェアの使用に際して起こりうる副作用について、著作権者ならび配布者は何らの保証をも行うものではありません。

■既知の制約事項
変換対象のtest.ldrは、ローカルドライブ上に存在しなければなりません。
